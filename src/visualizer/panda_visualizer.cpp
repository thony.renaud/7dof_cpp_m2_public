#include "panda_visualizer.h"
#include <thread>
#include <unistd.h>

pandev::visualizer::visualizer(const bool & stop_by_the_endIn, const bool & already_runningIn):
  _port                           ( 19997 ),
  _waitUntilConnected             ( 0 ),
  _doNotReconnectOnceDisconnected ( 1 ),
  _timeOutInMs                    ( 1000 ),
  _commThreadCycleInMs            ( 5 ),
  _ftHandle                       ( 43 ),
  _laser_offset                   ( 37. ),
  index                           ( 0   ),
  stop_by_the_end                 ( stop_by_the_endIn ),
  already_running                 ( already_runningIn )
{
  _laser_handle = 38;  
  _jointHandle = {17, 20, 23, 25, 27, 29, 31};
  _q_vector.resize(7);
  _client_id = simxStart("127.0.0.1", 
                              _port,
                              _waitUntilConnected,
                              _doNotReconnectOnceDisconnected, 
                              _timeOutInMs, 
                              _commThreadCycleInMs);
  // simxStopSimulation(_client_id, simx_opmode_oneshot_wait);
  // std::this_thread::sleep_for(std::chrono::seconds(6));
  if (!already_running){
    simstart_result = simxStartSimulation(_client_id, simx_opmode_oneshot_wait);
    // std::this_thread::sleep_for(std::chrono::seconds(10));
    // setPositions(q_0_v);
  }
}

pandev::visualizer::~visualizer(){
  std::cout << "[visualizer] closing ...\n";
  if (stop_by_the_end && simstart_result == 0){
    simxStopSimulation(_client_id, simx_opmode_oneshot_wait);
    simxFinish(_client_id);
  }
  if(t1.joinable())
    t1.join();
  std::cout << "[visualizer] closed ...\n";
}

void pandev::visualizer::setPositions(const Eigen::VectorXd &q){
  for (int i = 0; i<7; i++){
    _q_vector[i] = q(i);
  }
  index = 0;
  simxPauseCommunication(_client_id,(simxUChar)1);
  for(auto joint : _jointHandle){
    simxSetJointPosition(_client_id, joint, (simxFloat)_q_vector.at(index++), 0);
  }
  simxPauseCommunication(_client_id,(simxUChar)0);  
}