#include "trajectory_generation.h"

Polynomial::Polynomial(){};

Polynomial::Polynomial(const double &piIn, const double &pfIn, const double & DtIn){
  //Update all coefficients
  update(piIn, pfIn, DtIn);
};

void          Polynomial::update(const double &piIn, const double &pfIn, const double & DtIn){
  //Get all coefficient each time
  a[0] = piIn;
  a[1] = 0;
  a[2] = 0;
  a[3] = (10/(pow(DtIn, 3)))*(pfIn - piIn);
  a[4] = (-15/(pow(DtIn, 4)))*(pfIn - piIn);
  a[5] = (6/(pow(DtIn, 5)))*(pfIn - piIn);
};


const double  Polynomial::p     (const double &t){
  //Compute position
  const double p = a[0] + a[1]*t + a[2]*pow(t, 2) + a[3]*pow(t, 3) + a[4]*pow(t, 4) + a[5]*pow(t, 5);
  return p;
};

const double  Polynomial::dp    (const double &t){
  //Compute velocity
  const double dp = a[1] + 2*a[2]*t + 3*a[3]*pow(t, 2) + 4*a[4]*pow(t, 3) + 5*a[5]*pow(t, 4);
  return dp;
};

Point2Point::Point2Point(const Eigen::Affine3d & X_i, const Eigen::Affine3d & X_f, const double & DtIn){
  //Initialize object and polynomials
  polx.update(X_i.translation()(0), X_f.translation()(0), DtIn); //Polynom for x translation
  poly.update(X_i.translation()(1), X_f.translation()(1), DtIn); //Polynom for y translation
  polz.update(X_i.translation()(2), X_f.translation()(2), DtIn); //Polynom for z translation
  Eigen::Matrix3d Rot_i = X_i.rotation();
  Eigen::Matrix3d Rot_f = X_f.rotation();
  rot_aa = Rot_i.transpose() * Rot_f; 
  axis = rot_aa.axis();
  pol_angle.update(0, rot_aa.angle(), DtIn); //Polynom for rotation Matrix
}

Eigen::Affine3d Point2Point::X(const double & time){
  Eigen::Affine3d XD;

  //Transformation Matrix 4x4 for position
  XD.translation()(0) = polx.p(time);
  XD.translation()(1) = poly.p(time);
  XD.translation()(2) = polz.p(time);
  Eigen::AngleAxisd MRot(pol_angle.p(time), axis);
  XD.linear() = MRot.matrix();

  return XD;
}

Eigen::Vector6d Point2Point::dX(const double & time){
  Eigen::Vector6d VD;
  //Transformation Matrix 4x4 for velocity
  VD(0) = polx.dp(time);
  VD(1) = poly.dp(time);
  VD(2) = polz.dp(time);
  VD(3) = axis(0)*pol_angle.dp(time);
  VD(4) = axis(1)*pol_angle.dp(time);
  VD(5) = axis(2)*pol_angle.dp(time);

  return VD;
}