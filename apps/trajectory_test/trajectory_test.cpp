#include "trajectory_generation.h"
#include <iostream>


int main(){  

const double tf = 2.0;
const double dt = 0.1;
Eigen::Affine3d X_i;
Eigen::Affine3d X_f;
Eigen::Affine3d Xa;
Eigen::Vector6d Va;
Eigen::Vector3d Pos;
Eigen::AngleAxisd Rot    {   Eigen::AngleAxisd::Identity() };
Eigen::Vector3d Angles;
Eigen::Vector3d vi(0.1, 0.2, 0.3);
Eigen::Vector3d vf(0.2, 0.3, 0.4);


//Initial transformation matrix
X_i.translation()(0) = (1);
X_i.translation()(1) = (2);
X_i.translation()(2) = (3);
Eigen::AngleAxisd dirangle(M_PI_4, vi);
X_i.linear() = dirangle.matrix();


//Final transformation matrix
X_f.translation()(0) = (4);
X_f.translation()(1) = (1);
X_f.translation()(2) = (1);
Eigen::AngleAxisd diranglef(M_PI_2, vf);
X_f.linear() = diranglef.matrix();


//Declaration of TrajRot to get all coef
Point2Point TrajRot(X_i, X_f, tf);

std::cout<< "t x y z rx ry rz vx vy vz vrx vry vrz" << std::endl;

for (double i = 0.0; i <= tf+dt; i = i + dt){ 
  
  Xa = TrajRot.X(i); //Actual transformation Matrix
  Pos = Xa.translation(); //X, Y, Z
  Rot = Xa.rotation(); //Actual rotation matrix
  Angles = Rot.axis(); //Rx, Ry, Rz

  Va = TrajRot.dX(i); //Velocity on x, y, z, rx, ry, rz


  std::cout<< i << " " << Pos(0) << " " << Pos(1) << " " << Pos(2) << " " << Angles(0) << " " << Angles(1) << " " << Angles(2) 
  << " " << Va(0) << " " << Va(1) << " " << Va(2) << " " << Va(3) << " " << Va(4) << " " << Va(5) << std::endl;



}
}