#include "panda_visualizer.h"

#include <iostream>
#include <array>
#include <algorithm>


int main(){
  pandev::visualizer v;
  const int joint2move = 1;  
  Eigen::VectorXd q0(7);  
  q0 << -0.479698, 0.0723513 ,-0.012116  ,-1.99778  , 1.32045  , 1.14296 ,-0.554182;
  Eigen::VectorXd q(7);
  q = q0;
  for ( int i = 0; i < 1e3; i++ ){
    q(joint2move) = q0(joint2move) + 0.6*sin(i*0.01);
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    v.setPositions(q);        
  }
  return 0;
}
